#!/bin/sh
echo "node version ------------------------------------------";
node -v;

echo "npm version -------------------------------------------";
npm -v;

echo "yarn version ------------------------------------------";
yarn -v;

echo "environment -------------------------------------------";
printenv;

# constants
export DEFAULT_USE_YARN="false";

# configurables
export USE_YARN=${USE_YARN:-${DEFAULT_USE_YARN}};

# derived

# debug
echo "USE_YARN: ${USE_YARN}";
echo "find help at - https://gitlab.com/usvc/images/ci/recipes#js-build";

if [ "${USE_YARN}" = "true" ]; then
  yarn build;
else
  npm run build;
fi;
