DOCKER_NAMESPACE=usvc
DOCKER_IMAGE=ci
TIMESTAMP=$(shell date +'%Y%m%d%H%M%S')
ifndef ${CI_COMMIT_SHORT_SHA}
	GIT_COMMIT=$(shell git rev-parse --verify HEAD | head -c 8)-dev-$(TIMESTAMP)
else
	GIT_COMMIT=${CI_COMMIT_SHORT_SHA}
endif

-include Makefile.properties

build_all:
	@$(MAKE) all ACTION=build
test_all:
	@$(MAKE) all ACTION=test
save_all:
	@$(MAKE) all ACTION=save
release_all:
	@$(MAKE) all ACTION=release
publish_all:
	@$(MAKE) all ACTION=publish

all:
	find . \
		-maxdepth 1 \
		-type d \
		\( -iname '*' ! -iname '.*' \) | cut -d '/' -f 2 \
		| xargs -I@ \
			bash -c "make ${ACTION} SRC=@"
build:
	docker build \
		--file ./${SRC}/Dockerfile \
		--tag $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):${SRC} \
		./${SRC}
test: build
	container-structure-test test \
		--config ./${SRC}/Dockerfile.yaml \
		--image $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):${SRC}
save: test
	mkdir -p ./.build
	docker save \
		--output ./.build/${SRC}.tar.gz \
		$(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):${SRC}
release: test
	docker tag $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):${SRC} \
		$(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):${SRC}-${GIT_COMMIT}
publish: release
	docker push $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):${SRC}
	docker push $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):${SRC}-${GIT_COMMIT}

_debug:
	@echo "DOCKER_NAMESPACE:    $(DOCKER_NAMESPACE)"
	@echo "DOCKER_IMAGE:        $(DOCKER_IMAGE)"
	@echo "TIMESTAMP:           $(TIMESTAMP)"
	@echo "CI_COMMIT_SHORT_SHA: ${CI_COMMIT_SHORT_SHA}"
	@echo "GIT_COMMIT:          $(GIT_COMMIT)"
