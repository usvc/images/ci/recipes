module github.com/usvc/ci

go 1.13

require (
	github.com/golang/protobuf v1.3.1
	github.com/spf13/cobra v0.0.6
	github.com/stretchr/testify v1.2.2
	github.com/usvc/logger v0.2.2
	google.golang.org/grpc v1.21.0
)
