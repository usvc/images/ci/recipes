package main

import (
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/suite"
)

type MainTests struct {
	suite.Suite
}

func TestMain(t *testing.T) {
	suite.Run(t, &MainTests{})
}

func (s *MainTests) Test_main() {
	if os.Getenv("EXIT_FLAG") == "1" {
		main()
		return
	}
	s.NotNil(log)
	s.NotNil(cmd)
	cmd := exec.Command(os.Args[0], "-test.run=TestMain")
	cmd.Env = append(os.Environ(), "EXIT_FLAG=1")
	output, err := cmd.Output()
	e, ok := err.(*exec.ExitError)
	s.True(ok)
	s.False(e.Success())
	s.Equal(ExitCodeOfExitCodes, e.ExitCode())
	s.Contains(string(output), "@message=\"app - hello world\"")
}
