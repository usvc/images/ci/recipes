package main

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/usvc/logger"
)

const (
	ExitCodeOfExitCodes = 42
)

var (
	cmd *cobra.Command
	log logger.Logger
)

func init() {
	log = logger.New(logger.Options{})
	cmd = &cobra.Command{
		Use: "app",
		Run: func(command *cobra.Command, args []string) {
			log.Info("app - hello world")
			os.Exit(ExitCodeOfExitCodes)
		},
	}
}

func main() {
	cmd.Execute()
}
