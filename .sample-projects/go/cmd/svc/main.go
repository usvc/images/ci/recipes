package main

import (
	"github.com/spf13/cobra"
	"github.com/usvc/logger"
)

var (
	cmd *cobra.Command
	log logger.Logger
)

func init() {
	log = logger.New(logger.Options{})
	cmd = &cobra.Command{
		Use: "svc",
		Run: func(command *cobra.Command, args []string) {
			log.Info("svc - hello world")
		},
	}
}

func main() {
	cmd.Execute()
}
