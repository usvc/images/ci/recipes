#!/bin/sh
go version;
go env;
protoc --version;

# constants
export DEFAULT_PROTO_FILENAME=specs;

# configurables
export PROTO_DIR=${PROTO_DIR};
export PROTO_FILENAME=${PROTO_FILENAME:-${DEFAULT_PROTO_FILENAME}};

# debug
printf -- 'debug: \n';
echo "PROTO_DIR: ${PROTO_DIR}";
echo "PROTO_FILENAME: ${PROTO_FILENAME}";

if [ "${PROTO_DIR}" = "" ]; then
  printf -- 'PROTO_DIR was not defined but is required.\n';
  exit 1;
fi;

protoc \
  --proto_path=${PROTO_DIR} \
  --go_out=plugins=grpc:${PROTO_DIR} \
  ./${PROTO_DIR}/${PROTO_FILENAME}.proto;
