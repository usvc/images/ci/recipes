#!/bin/bash

# constants
export DEFAULT_GITLAB_HOSTNAME=gitlab.com;
export BUMP_MAJOR_LABEL='\[bump major\]';
export BUMP_MINOR_LABEL='\[bump minor\]';
export BUMP_PRERELEASE_LABEL='\[bump prerelease\]';

# configurables
export COMMIT_MESSAGE=${CI_COMMIT_MESSAGE:-${COMMIT_MESSAGE}};
export DEPLOY_KEY=${DEPLOY_KEY};
export DEPLOY_KEY_MD5SUM=$(printf -- '${DEPLOY_KEY}' | md5sum);
export GITLAB_HOSTNAME=${GITLAB_HOSTNAME:-${DEFAULT_GITLAB_HOSTNAME}};
export PROJECT_PATH=${CI_PROJECT_PATH:-${PROJECT_PATH}};

# debug
printf -- 'debug: \n';
echo "BUMP_MAJOR_LABEL: ${BUMP_MAJOR_LABEL}";
echo "BUMP_MINOR_LABEL: ${BUMP_MINOR_LABEL}";
echo "BUMP_PRERELEASE_LABEL: ${BUMP_PRERELEASE_LABEL}";
echo "COMMIT_MESSAGE: ${COMMIT_MESSAGE}";
echo "DEPLOY_KEY: ${DEPLOY_KEY_MD5SUM}";
echo "GITLAB_HOSTNAME: ${GITLAB_HOSTNAME}";
echo "PROJECT_PATH: ${PROJECT_PATH}";

if [ "${DEPLOY_KEY}" = "" ]; then
  printf -- 'DEPLOY_KEY not defined - this is required to authenticate with the remote\n';
  exit 1;
elif [ "${PROJECT_PATH}" = "" ]; then
  printf -- 'PROJECT_PATH not defined - this is required to know where to push to\n';
  exit 1;
fi;

if [ "${CI}" != "" ]; then
  printf -- 'adding the provided DEPLOY_KEY to the known hosts and prepare git repository for version tagging in 3 seconds... (hit ctrl+c to abort)\n';
  sleep 3;
  mkdir -p ~/.ssh;
  printf -- "${DEPLOY_KEY}" | base64 -d > ~/.ssh/id_rsa;
  chmod 600 -R ~/.ssh/id_rsa;
  ssh-keyscan -t rsa ${GITLAB_HOSTNAME} >> ~/.ssh/known_hosts;
  git remote set-url origin "git@${GITLAB_HOSTNAME}:${CI_PROJECT_PATH}.git";
  git checkout master;
  git fetch;
else
  printf -- 'skipped preparing git repository because not running in CI pipeline...\n';
fi;

VERSION="patch"
case "$COMMIT_MESSAGE" in
  *${BUMP_MAJOR_LABEL}*)
    VERSION="major";
  ;;
  *${BUMP_MINOR_LABEL}*)
    VERSION="minor";
  ;;
  *${BUMP_PRERELEASE_LABEL}*)
    VERSION="prerelease";
  ;;
esac;
if [ "${CI}" != "" ]; then
  printf -- "bumping ${VERSION} version in 3 seconds... (hit ctrl+c to abort)\n";
  sleep 3;
  semver bump ${VERSION} --git --apply;
  printf -- "pushing to git repository...\n";
  git push origin master --verbose --tags;
  printf -- "cleaning up environment...\n";
  rm -rf ~/.ssh/*;
else
  printf -- 'skipped preparing git repository because not running in CI pipeline...\n';
  semver bump ${VERSION} --git;
fi;
