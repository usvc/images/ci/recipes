#!/bin/sh

# constants
export DEFAULT_MODULE_URL=`cat go.mod | head -n 1 | cut -f 2 -d ' '`;

# configurables
export CC_TEST_REPORTER_ID=${CC_TEST_REPORTER_ID};
export CC_TEST_REPORTER_ID_MD5=$(printf -- ${CC_TEST_REPORTER_ID} | md5sum);
export MODULE_URL=${MODULE_URL:-${DEFAULT_MODULE_URL}};

# debug
echo "CC_TEST_REPORTER_ID: ${CC_TEST_REPORTER_ID_MD5}";
echo "MODULE_URL: ${MODULE_URL}";
echo "find help at - https://gitlab.com/usvc/images/ci/recipes#go-coverage-code-climate"

if [ "${CC_TEST_REPORTER_ID}" = "" ]; then
  printf -- 'CC_TEST_REPORTER_ID was not defined but is required.\n';
  exit 1;
fi;

cc-test-reporter before-build;
cc-test-reporter format-coverage --debug --prefix "${MODULE_URL}" --input-type gocov c.out;
cc-test-reporter upload-coverage --debug;
cc-test-reporter after-build --debug --prefix "${MODULE_URL}";
