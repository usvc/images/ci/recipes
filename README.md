# CI Recipes

[![pipeline status](https://gitlab.com/usvc/images/ci/recipes/badges/master/pipeline.svg)](https://gitlab.com/usvc/images/ci/recipes/-/commits/master)

Overview of recipes:
- [CI Recipes](#ci-recipes)
- [Projects In Use](#projects-in-use)
- [CI Templates](#ci-templates)
  - [Gitlab CI Templates](#gitlab-ci-templates)
    - [Go Packages](#go-packages)
- [Images](#images)
  - [compress](#compress)
    - [compress configurations](#compress-configurations)
    - [compress outputs](#compress-outputs)
    - [compress sample scripts](#compress-sample-scripts)
      - [compress in Gitlab](#compress-in-gitlab)
  - [docker-build](#docker-build)
    - [docker-build configurations](#docker-build-configurations)
    - [docker-build outputs](#docker-build-outputs)
    - [docker-build sample scripts](#docker-build-sample-scripts)
      - [docker-build in Gitlab](#docker-build-in-gitlab)
  - [docker-publish](#docker-publish)
    - [docker-publish configurations](#docker-publish-configurations)
    - [docker-publish outputs](#docker-publish-outputs)
    - [docker-publish sample scripts](#docker-publish-sample-scripts)
      - [docker-publish in Gitlab](#docker-publish-in-gitlab)
  - [go-build](#go-build)
    - [go-build configurations](#go-build-configurations)
    - [go-build outputs](#go-build-outputs)
    - [go-build sample scripts](#go-build-sample-scripts)
      - [go-build in Makefile](#go-build-in-makefile)
      - [go-build in Gitlab](#go-build-in-gitlab)
  - [go-build-production](#go-build-production)
    - [go-build-production configurations](#go-build-production-configurations)
    - [go-build-production outputs](#go-build-production-outputs)
    - [go-build-production sample scripts](#go-build-production-sample-scripts)
      - [go-build in Makefile](#go-build-in-makefile-1)
      - [go-build-production in Gitlab](#go-build-production-in-gitlab)
  - [go-coverage-code-climate](#go-coverage-code-climate)
    - [go-coverage-code-climate outputs](#go-coverage-code-climate-outputs)
    - [go-coverage-code-climate configurations](#go-coverage-code-climate-configurations)
    - [go-coverage-code-climate sample scripts](#go-coverage-code-climate-sample-scripts)
      - [go-coverage-code-climate in Gitlab](#go-coverage-code-climate-in-gitlab)
  - [go-dependencies](#go-dependencies)
    - [go-dependencies outputs](#go-dependencies-outputs)
    - [go-dependencies sample scripts](#go-dependencies-sample-scripts)
      - [go-dependencies in Gitlab](#go-dependencies-in-gitlab)
  - [go-protobuf](#go-protobuf)
    - [go-protobuf outputs](#go-protobuf-outputs)
    - [go-protobuf configurations](#go-protobuf-configurations)
    - [go-protobuf sample scripts](#go-protobuf-sample-scripts)
      - [go-protobuf in Gitlab](#go-protobuf-in-gitlab)
  - [go-test](#go-test)
    - [go-test outputs](#go-test-outputs)
    - [go-test configurations](#go-test-configurations)
    - [go-test sample scripts](#go-test-sample-scripts)
      - [go-test in Gitlab](#go-test-in-gitlab)
  - [js-build](#js-build)
    - [js-build outputs](#js-build-outputs)
    - [js-build configurations](#js-build-configurations)
    - [js-build sample scripts](#js-build-sample-scripts)
      - [js-build in Gitlab](#js-build-in-gitlab)
  - [js-dependencies](#js-dependencies)
    - [js-dependencies outputs](#js-dependencies-outputs)
    - [js-dependencies configurations](#js-dependencies-configurations)
    - [js-dependencies sample scripts](#js-dependencies-sample-scripts)
      - [js-dependencies in Gitlab](#js-dependencies-in-gitlab)
  - [version-bump-gitlab](#version-bump-gitlab)
    - [version-bump-gitlab configurations](#version-bump-gitlab-configurations)
    - [version-bump-gitlab outputs](#version-bump-gitlab-outputs)
    - [version-bump-gitlab sample scripts](#version-bump-gitlab-sample-scripts)
      - [version-bump-gitlab in Gitlab](#version-bump-gitlab-in-gitlab)
- [Development Runbook](#development-runbook)
  - [Getting Started](#getting-started)
  - [CI Configuration](#ci-configuration)
    - [Provisioning a Docker Hub access token](#provisioning-a-docker-hub-access-token)
- [License](#license)


- - -


# Projects In Use

| Name | Repository | Pipeline |
| --- | --- | --- |
| `usvc/go-config` | [`github.com/usvc/go-config](https://github.com/usvc/go-config) | [On Gitlab](https://gitlab.com/usvc/modules/go/config/pipelines) |
| `usvc/go-db` | [`github.com/usvc/go-db](https://github.com/usvc/go-db) | [On Gitlab](https://gitlab.com/usvc/modules/go/db/pipelines) |
| `usvc/go-logger` | [`github.com/usvc/go-logger](https://github.com/usvc/go-logger) | [On Gitlab](https://gitlab.com/usvc/modules/go/logger/pipelines) |
| `usvc/go-password` | [`github.com/usvc/go-password](https://github.com/usvc/go-password) | [On Gitlab](https://gitlab.com/usvc/modules/go/password/pipelines) |
| `usvc/go-semver` | [`github.com/usvc/go-semver](https://github.com/usvc/go-semver) | [On Gitlab](https://gitlab.com/usvc/modules/go/semver/pipelines) |


- - -


# CI Templates

## Gitlab CI Templates

Gitlab CI includes a way to remotely use an existing pipeline that's been defined in YAML [through the `include` property](https://docs.gitlab.com/ee/ci/yaml/README.html#include).

### Go Packages

The template pipeline for Go projects can be found at [`./templates/go-package.yml`](./templates/go-package.yml) and can be included using:

```yaml
# ...
include: 
  - remote: 'https://gitlab.com/usvc/images/ci/recipes/raw/master/.templates/go-package.yml'
# ...
```


- - -


# Images

## `compress`

**Image URL**: [`usvc/ci:compress`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=compress)

`compress` uses the `upx` tool to compress your binaries.

To run locally, use:

```sh
make test SRC=compress;
# for compressing linux binaries
docker run -it --volume $(pwd)/.sample-projects/go:/app -e BIN_NAME=app_linux_amd64 --workdir /app --user $(id -u) usvc/ci:compress entrypoint;
# test out the linux binary
./.sample-projects/go/bin/app_linux_amd64
# for compressing windows pes (demonstrating BIN_EXT)
docker run -it --volume $(pwd)/.sample-projects/go:/app -e BIN_NAME=app_windows_386 -e BIN_EXT=.exe --workdir /app --user $(id -u) usvc/ci:compress entrypoint;
```

> Script can be found at: [`./compress/entrypoint.sh`](./compress/entrypoint.sh)

### `compress` configurations

| Key | Default | Description |
| --- | --- | --- |
| `BIN_EXT` | `""` | Extension of the binary |
| `BIN_NAME` | `"app"` | Relative path to the binary to be compressed (binary should be in `./bin`) |
| `BIN_PATH` | `"./bin"` | Relative or absolute path to the directory where `./${BIN_NAME}` should exist the binary to compress |

### `compress` outputs

- on failure, the dotfile will be left behind at `./bin/.*`
- on success, the binary will be overwritten at `./bin/*`

### `compress` sample scripts

#### `compress` in Gitlab

```yaml
.compress:
  stage: package
  image: usvc/ci:compress
  script: [entrypoint]
  only: [tags]
  artifacts:
    paths: ["./bin/*"]
  allow_failure: true
compress linux:
  extends: [.compress, .go_linux]
  dependencies: [build linux]
compress macos:
  extends: [.compress, .go_macos]
  dependencies: [build macos]
compress windows:
  extends: [.compress, .go_windows]
  dependencies: [build windows]

.go_linux:
  variables:
    GOOS: linux
    GOARCH: amd64
.go_macos:
  variables:
    GOOS: darwin
    GOARCH: amd64
.go_windows:
  variables:
    GOOS: windows
    GOARCH: "386"
```


- - -


## `docker-build`

**Image URL**: [`usvc/ci:docker-build`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=docker-build)

`docker-build` builds and tests.

To run locally, use:

```sh
make test SRC=docker-build;

```

> Script can be found at: [`./docker-build/entrypoint.sh`](./docker-build/entrypoint.sh)

### `docker-build` configurations

| Key | Default | Description |
| --- | --- | --- |
| `DOCKER_CONTEXT_PATH` | `"./"` | Defines the build context of the Docker build |
| `DOCKER_IMAGE_TAG` | `"latest"` | The image tag (after the `:`) |
| `DOCKER_IMAGE_URL` | `""` | The image url (before the `:`) |
| `DOCKERFILE_PATH` | `"./deploy/Dockerfile"` | Defines the path to the Dockerfile from the project root |
| `OUTPUT_DIRECTORY` | `"./build"` | Defines the output directory of the `docker save` command |
| `OUTPUT_FILENAME` | `"output.tar.gz"` | Defines the output filename of the `docker save` command |
| `TEST_SPEC_PATH` | `"./deploy/Dockerfile.yaml"` | Defines the path to the `container-structure-test` specification from the project root |

### `docker-build` outputs

- `${OUTPUT_DIRECTORY}/${OUTPUT_FILENAME}` *(if left to defaults, this is `./build/output.tar.gz`)*

### `docker-build` sample scripts

#### `docker-build` in Gitlab

```yaml
dockerize:
  stage: package
  only: ["tags"]
  services: ["docker:19.03.1-dind"]
  image: usvc/ci:docker-build
  artifacts:
    paths: ["./build/*"]
  variables:
    DOCKER_IMAGE_URL: usvc/echoserver
  script: ["entrypoint"]
```


- - -


## `docker-publish`

**Image URL**: [`usvc/ci:docker-publish`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=docker-publish)

`docker-publish` publishes the built image using the output from `docker-build`.

To run locally, use:

```sh
make test SRC=docker-publish;
```

> Script can be found at: [`./docker-publish/entrypoint.sh`](./docker-publish/entrypoint.sh)

### `docker-publish` configurations

| Key | Default | Description |
| --- | --- | --- |
| `DOCKER_IMAGE_TAG` | `"latest"` | The image tag (after the `:`) |
| `DOCKER_IMAGE_URL` | `""` | The image url (before the `:`) |
| `DOCKER_REGISTRY_HOSTNAME` | `"docker.io"` | The hostname of the Docker registry |
| `DOCKER_REGISTRY_USERNAME` | `""` | Username of the Docker registry user |
| `DOCKER_REGISTRY_PASSWORD` | `""` | Password of the Docker registry user whose username is `DOCKER_REGISTRY_USERNAME` |
| `INPUT_DIRECTORY` | `"./build"` | Defines the input directory for the `docker load` command |
| `INPUT_FILENAME` | `"output.tar.gz"` | Defines the input filename for the `docker load` command |

### `docker-publish` outputs

None.

### `docker-publish` sample scripts

#### `docker-publish` in Gitlab

```yaml
dockerhub:
  stage: publish
  only: ["tags"]
  services: ["docker:19.03.1-dind"]
  image: usvc/ci:docker-publish
  dependencies: ["dockerize"]
  variables:
    DOCKER_IMAGE_URL: usvc/echoserver
  script: ["entrypoint"]
```

- - -


## `go-build`

**Image URL**: [`usvc/ci:go-build`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=go-build)

To run locally, use:

```sh
make test SRC=go-build;
# building another command
docker run -it --volume $(pwd)/.sample-projects/go:/app -e CMD_ROOT=svc --workdir /app --user $(id -u) usvc/ci:go-build entrypoint;
# building for linux
docker run -it --volume $(pwd)/.sample-projects/go:/app --workdir /app --user $(id -u) usvc/ci:go-build entrypoint;
# building for macos
docker run -it --volume $(pwd)/.sample-projects/go:/app -e GOOS=darwin -e GOARCH=amd64 --workdir /app --user $(id -u) usvc/ci:go-build entrypoint;
# building for windows
docker run -it --volume $(pwd)/.sample-projects/go:/app -e GOOS=windows -e GOARCH=386 -e BIN_EXT=.exe --workdir /app --user $(id -u) usvc/ci:go-build entrypoint;
```

> Script can be found at: [`./go-build/entrypoint.sh`](./go-build/entrypoint.sh)

### `go-build` configurations

| Key | Default | Description |
| --- | --- | --- |
| `BIN_EXT` | `""` | Extension of the binary |
| `BIN_NAME` | `"app"` | Filename of the binary |
| `BIN_PATH` | `"./bin"` | Path to directory where the binary should be output |
| `CGO_ENABLED` | `0` | Indicates whether CGO should be enabled in the build |
| `GOOS` | `$(go env GOOS)` | Operating system to build for |
| `GOARCH` | `$(go env GOARCH)` | Architecture to build for |
| `STATIC` | `0` | Should be either 0 or 1. When this is set to 1, static linking is attempted |
| `USE_CACHE` | `0` | Should be either 0 or 1. When this is set to 1, `go build` will use the cache |

### `go-build` outputs

- `${BIN_PATH}/${BIN_NAME}_${GOOS}_${GOARCH}${BIN_EXT}` *(default if left unset is `./bin/app_linux_amd64`)*

### `go-build` sample scripts

#### `go-build` in Makefile

You can use `usvc/ci:go-build` via the Docker image in a Makefile as such:

```Makefile
# this directory should exist in the ./cmd directory relative to the
# project root. for example in this instance, ./cmd/app, should contain
# your code's main package
CMD_NAME=app

# this defines where the package should go inside the image, this shouldn't
# be important from Go 1.12 onwards because of Go Modules
GO_PACKAGE=github.com/user/reponame

build:
  docker run -it \
		--env BIN_NAME=$(CMD_NAME) \
		--user $$(id -u) \
		--volume $$(pwd):/go/$(GO_PACKAGE) \
		--workdir /go/$(GO_PACKAGE) \
		usvc/ci:go-build entrypoint
```

#### `go-build` in Gitlab

```yaml
.build:
  stage: test & build
  image: usvc/ci:go-build
  dependencies: ["init"]
  artifacts:
    paths: ["./bin/*"]
  variables:
    BIN_NAME: %__REPLACE_WITH_YOUR_CMD_NAME__
  before_script: ["git fetch"]
  script: ["entrypoint"]
build linux (test):
  extends: [.build, .go_linux]
  only: [master]
build linux:
  extends: [.build, .go_linux]
  only: [tags]
build macos (test):
  extends: [.build, .go_macos]
  only: [master]
build macos:
  extends: [.build, .go_macos]
  only: [tags]
build windows (test):
  extends: [.build, .go_windows]
  only: [master]
build windows:
  extends: [.build, .go_windows]
  only: [tags]

.go_linux:
  variables:
    GOOS: linux
    GOARCH: amd64
.go_macos:
  variables:
    GOOS: darwin
    GOARCH: amd64
.go_windows:
  variables:
    GOOS: windows
    GOARCH: "386"
```

- - -


## `go-build-production`

**Image URL**: [`usvc/ci:go-build-production`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=go-build-production)

To run locally, use:

```sh
make test SRC=go-build-production;
# building another command
docker run -it --volume $(pwd)/.sample-projects/go:/app -e CMD_ROOT=svc --workdir /app --user $(id -u) usvc/ci:go-build-production entrypoint;
# building for linux
docker run -it --volume $(pwd)/.sample-projects/go:/app --workdir /app --user $(id -u) usvc/ci:go-build-production entrypoint;
# building for macos
docker run -it --volume $(pwd)/.sample-projects/go:/app -e GOOS=darwin -e GOARCH=amd64 --workdir /app --user $(id -u) usvc/ci:go-build-production entrypoint;
# building for windows
docker run -it --volume $(pwd)/.sample-projects/go:/app -e GOOS=windows -e GOARCH=386 -e BIN_EXT=.exe --workdir /app --user $(id -u) usvc/ci:go-build-production entrypoint;
```

> Script can be found at: [`./go-build-production/entrypoint.sh`](./go-build-production/entrypoint.sh)

### `go-build-production` configurations

| Key | Default | Description |
| --- | --- | --- |
| `BIN_EXT` | `""` | Extension of the binary |
| `BIN_NAME` | `"app"` | Filename of the binary |
| `BIN_PATH` | `"./bin"` | Path to directory where the binary should be output |
| `CGO_ENABLED` | `0` | Indicates whether CGO should be enabled in the build |
| `GOOS` | `$(go env GOOS)` | Operating system to build for |
| `GOARCH` | `$(go env GOARCH)` | Architecture to build for |
| `STATIC` | `0` | Should be either 0 or 1. When this is set to 1, static linking is attempted |
| `USE_CACHE` | `0` | Should be either 0 or 1. When this is set to 1, `go build` will use the cache |

### `go-build-production` outputs

- `${BIN_PATH}/${BIN_NAME}_${GOOS}_${GOARCH}${BIN_EXT}` *(default if left unset is `./bin/app_linux_amd64`)*

### `go-build-production` sample scripts

#### `go-build` in Makefile

You can use `usvc/ci:go-build-production` via the Docker image in a Makefile as such:

```Makefile
# this directory should exist in the ./cmd directory relative to the
# project root. for example in this instance, ./cmd/app, should contain
# your code's main package
CMD_NAME=app

# this defines where the package should go inside the image, this shouldn't
# be important from Go 1.12 onwards because of Go Modules
GO_PACKAGE=github.com/user/reponame

build_production:
	docker run -it \
		--env BIN_NAME=$(CMD_NAME) \
		--env USE_CACHE=0 \
		--user $$(id -u) \
		--volume $$(pwd):/go/$(GO_PACKAGE) \
		--workdir /go/$(GO_PACKAGE) \
		usvc/ci:go-build-production entrypoint
```

#### `go-build-production` in Gitlab

```yaml
.build:
  stage: test & build
  image: usvc/ci:go-build-production
  dependencies: ["init"]
  artifacts:
    paths: ["./bin/*"]
  variables:
    BIN_NAME: %__REPLACE_WITH_YOUR_CMD_NAME__
  before_script: ["git fetch"]
  script: ["entrypoint"]
build linux (test):
  extends: [.build, .go_linux]
  only: [master]
build linux:
  extends: [.build, .go_linux]
  only: [tags]
build macos (test):
  extends: [.build, .go_macos]
  only: [master]
build macos:
  extends: [.build, .go_macos]
  only: [tags]
build windows (test):
  extends: [.build, .go_windows]
  only: [master]
build windows:
  extends: [.build, .go_windows]
  only: [tags]

.go_linux:
  variables:
    GOOS: linux
    GOARCH: amd64
.go_macos:
  variables:
    GOOS: darwin
    GOARCH: amd64
.go_windows:
  variables:
    GOOS: windows
    GOARCH: "386"
```


- - -


## `go-coverage-code-climate`

**Image URL**: [`usvc/ci:go-coverage-code-climate`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=go-coverage-code-climate)

`go-coverage-code-climate` uploads Go coverage reports to Code Climate.

### `go-coverage-code-climate` outputs

- `coverage/codeclimate.json`

### `go-coverage-code-climate` configurations

| Key | Default | Description |
| --- | --- | --- |
| `CC_TEST_REPORTER_ID` | `""` | Test reporter ID found in your repository settings on Code Climate |
| `MODULE_URL` | *(retrieved from `go.mod`)* | Import path of your Go module |

### `go-coverage-code-climate` sample scripts

#### `go-coverage-code-climate` in Gitlab

```yaml
coverage report:
  stage: release
  dependencies: ["unit test"]
  image: usvc/ci:go-coverage-code-climate
  script: ["entrypoint"]
```


- - -


## `go-dependencies`

**Image URL**: [`usvc/ci:go-dependencies`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=go-dependencies)

`go-dependencies` uses Go Modules to pull in dependencies of your Go project.

To run locally, use:

```sh
make test SRC=go-dependencies;
docker run -it --volume $(pwd)/.sample-projects/go:/app --workdir /app --user $(id -u) usvc/ci:go-dependencies entrypoint;
```

> Script can be found at: [`./go-dependencies/entrypoint.sh`](./go-dependencies/entrypoint.sh)

### `go-dependencies` outputs

- `./vendor`

### `go-dependencies` sample scripts

#### `go-dependencies` in Gitlab

```yaml
init:
  stage: init
  image: usvc/ci:go-dependencies
  cache:
    key: ${CI_COMMIT_REF_NAME}
    paths: ["./vendor"]
  artifacts:
    paths: ["./vendor"]
  script: ["entrypoint"]
```


- - -


## `go-protobuf`

**Image URL**: [`usvc/ci:go-protobuf`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=go-protobuf)

`go-protobuf` generates `.go` files given a `.proto` file

To run locally, use:

```sh
make test SRC=go-protobuf;
docker run -it \
  --volume $(pwd)/.sample-projects/go:/app \
  --workdir /app \
  --user $(id -u) \
  --env PROTO_DIR=./internal/helloworld \
  --env PROTO_FILENAME=helloworld \
  usvc/ci:go-protobuf entrypoint;
```

> Script can be found at: [`./go-protobuf/entrypoint.sh`](./go-protobuf/entrypoint.sh)

### `go-protobuf` outputs

- `./vendor`

### `go-protobuf` configurations

| Key | Default | Description |
| --- | --- | --- |
| `PROTO_DIR` | `""` | Path to the directory containing the `.proto` file |
| `PROTO_FILENAME` | `"specs"` | Filename of the `.proto` file. |

### `go-protobuf` sample scripts

#### `go-protobuf` in Gitlab

```yaml
init:
  stage: init
  image: usvc/ci:go-protobuf
  cache:
    key: ${CI_COMMIT_REF_NAME}
    paths: ["./internal/helloworld/*.go"]
  artifacts:
    paths: ["./internal/helloworld/*.go"]
  variables:
    PROTO_DIR: ./internal/helloworld
    PROTO_FILENAME: helloworld
  script: ["entrypoint"]
```


- - -


## `go-test`

**Image URL**: [`usvc/ci:go-test`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=go-test)

`go-test` runs tests on all packages recursively from the working directory.

To run locally, use:

```sh
make test SRC=go-test;
docker run -it --volume $(pwd)/.sample-projects/go:/app --workdir /app --user $(id -u) usvc/ci:go-test entrypoint;
```

> Script can be found at: [`./go-test/entrypoint.sh`](./go-test/entrypoint.sh)

### `go-test` outputs

- `./c.out` (or `COVERPROFILE_PATH` if it's configured)

### `go-test` configurations

| Key | Default | Description |
| --- | --- | --- |
| `COVERPROFILE_PATH` | `"./c.out"` | Relative path to the test coverprofile file |
| `TEST_PATH` | `"./..."` | Directory/file path passed to `go test` for testing |

### `go-test` sample scripts

#### `go-test` in Gitlab

```yaml
unit test:
  stage: test & build
  image: usvc/ci:go-test
  dependencies: ["init"]
  artifacts:
    paths: ["./c.out"]
  script: ["entrypoint"]
```



- - -



## `js-build`

**Image URL**: [`usvc/ci:js-build`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=js-build)

`js-build` runs the build process for a JS project using the a `script` defined `build` in `package.json`.

### `js-build` outputs

- depends on script defined in the `build` script in `package.json`

### `js-build` configurations

| Key | Default | Description |
| --- | --- | --- |
| `USE_YARN` | `"false"` | Set to `"true"` to use `yarn` instead |

### `js-build` sample scripts

#### `js-build` in Gitlab

```yaml
dependencies:
  stage: init
  image: usvc/ci:js-build
  artifacts:
    paths: ["./build"]
  script: ["entrypoint"]
```



- - -



## `js-dependencies`

**Image URL**: [`usvc/ci:js-dependencies`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=js-dependencies)

`js-dependencies` installs dependencies for a JavaScript project.

### `js-dependencies` outputs

- `./node_modules`

### `js-dependencies` configurations

| Key | Default | Description |
| --- | --- | --- |
| `PRODUCTION` | `"true"` | Set to `"true"` to install only production dependencies |
| `USE_YARN` | `"false"` | Set to `"true"` to use `yarn` instead |

### `js-dependencies` sample scripts

#### `js-dependencies` in Gitlab

```yaml
dependencies:
  stage: init
  image: usvc/ci:js-dependencies
  artifacts:
    paths: ["./node_modules"]
  script: ["entrypoint"]
```



- - -


## `version-bump-gitlab`

**Image URL**: [`usvc/ci:version-bump-gitlab`](https://hub.docker.com/r/usvc/ci/tags?page=1&name=version-bump-gitlab)

`version-bump-gitlab` runs tests on all packages recursively from the working directory.

To run locally, use:

```sh
make test SRC=version-bump-gitlab;
```

> Script can be found at: [`./version-bump-gitlab/entrypoint.sh`](./version-bump-gitlab/entrypoint.sh)

### `version-bump-gitlab` configurations

| Key | Default | Description |
| --- | --- | --- |
| `GITLAB_HOSTNAME` | `"gitlab.com"` | Hostname of your gitlab installation |
| `BUMP_MAJOR_LABEL` | `"[bump major]"` | Sets the label that indicates the version bump should target the major section of the semver version|
| `BUMP_MINOR_LABEL` | `"[bump minor]"` | Sets the label that indicates the version bump should target the minor section of the semver version|
| `BUMP_PRERELEASE_LABEL` | `"[bump prerelease]"` | Sets the label that indicates the version bump should target the prerelease section of the semver version|
| `DEPLOY_KEY` | `""` | Base64-encoded string containing the private key corresponding to the public key set as a repository Deploy Key |
| `PROJECT_PATH` | `"${CI_PROJECT_PATH}"` | Project path of the repository for insertion after the Git remote URL `git@gitlab.com:${THIS}.git`; you shouldn't need to set this when running in Gitlab |

### `version-bump-gitlab` outputs

None.

### `version-bump-gitlab` sample scripts

#### `version-bump-gitlab` in Gitlab

```yaml
version bump:
  stage: release
  only: ["master"]
  image: usvc/ci:version-bump-gitlab
  script: ["entrypoint"]
```


- - -


# Development Runbook

## Getting Started

Make is used to organise development work.

We use four primary verbs to categorise operations doable to this repository:

1. `build` - builds the target image
2. `test` - runs `container-structure-test` against the target image
2. `release` - tags the target image with the current timestamp
2. `publish` - publishes the target image to the Docker registry

Each subsequent step will always call the previous step (eg. `publish` calls `release` as a dependent recipe, and `release` calls `test`, *et cetera*)

Target images are indicated by passing the `SRC` argument to the Makefile recipe. Example:

```sh
# builds the `go-build` image
make build SRC=go-build
# builds, tests, releases, and publishes, the `go-build` image
make publishes SRC=go-build
```

## CI Configuration

| Key | Description |
| --- | --- |
| `DOCKER_REGISTRY_HOSTNAME` | An optional hostname to use as the Docker registry host (defualts to `"docker.io"`) |
| `DOCKER_REGISTRY_USERNAME` | Username used to login to the intended Docker Registry to publish to |
| `DOCKER_REGISTRY_PASSWORD` | Password for the user used to login to the intended Docker Registry (use an access token, read the section on [Provisioning a Docker Hub access token](provisioning-a-docker-hub-access-token) for how to do that) |

### Provisioning a Docker Hub access token

Since September 2019, access tokens are available for provisioning in Docker Hub, to provision an access token, visit the security settings on your account at DockerHub (link: [https://hub.docker.com/settings/security](https://hub.docker.com/settings/security)) and click on **New Access Token**. Give it a name and copy/paste the output into the Gitlab CI/CD variables as `DOCKER_REGISTRY_PASSWORD`.


- - -


# License

Code is licensed under [the MIT license (click to read full text)](./LICENSE).
