#!/bin/sh
echo "go version --------------------------------------------";
go version;

echo "go environment ----------------------------------------";
go env;

# constants
export DEFAULT_BIN_PATH='./bin';
export DEFAULT_BIN_NAME='app';
export DEFAULT_CGO_ENABLED=0;
export DEFAULT_STATIC="0";
export DEFAULT_USE_CACHE="0";
export GIT_COMMIT=$(git rev-parse --verify HEAD);
export GIT_VERSION=$(git describe --tag $(git rev-list --tags --max-count=1));
export GO_ENV_GOARCH=$(go env GOARCH);
export GO_ENV_GOOS=$(go env GOOS);
export TIMESTAMP=$(date +'%Y%m%d%H%M%S');

# configurables
export BIN_EXT=${BIN_EXT};
export BIN_NAME=${BIN_NAME:-${DEFAULT_BIN_NAME}};
export BIN_PATH=${BIN_PATH:-${DEFAULT_BIN_PATH}};
export CGO_ENABLED=${CGO_ENABLED:-${DEFAULT_CGO_ENABLED}};
export GOOS=${GOOS:-${GO_ENV_GOOS}};
export GOARCH=${GOARCH:-${GO_ENV_GOARCH}};
export STATIC=${STATIC:-${DEFAULT_STATIC}};
export USE_CACHE=${USE_CACHE:-${DEFAULT_USE_CACHE}};

# derived
export BIN_FULL_FILENAME="${BIN_NAME}_${GOOS}_${GOARCH}${BIN_EXT}";
export BIN_FULL_PATH="${BIN_PATH}/${BIN_FULL_FILENAME}";
export BIN_FILENAME="${BIN_NAME}${BIN_EXT}";
export BIN_SYMLINK_PATH="${BIN_PATH}/${BIN_FILENAME}";
if [ "${STATIC}" = "1" ]; then
  export OTHER_LDFLAGS="-extldflags 'static'";
fi;
if [ "${USE_CACHE}" = "1" ]; then
  export OTHER_GO_BUILD_FLAGS="-a";
else
  export OTHER_GO_BUILD_FLAGS="";
fi;


# debug
echo "BIN_EXT:           ${BIN_EXT}";
echo "BIN_NAME:          ${BIN_NAME}";
echo "BIN_PATH:          ${BIN_PATH}";
echo "CGO_ENABLED:       ${CGO_ENABLED}";
echo "GOOS:              ${GOOS}";
echo "GOARCH:            ${GOARCH}";
echo "GIT_COMMIT:        ${GIT_COMMIT}";
echo "GIT_VERSION:       ${GIT_VERSION}";
echo "GO_ENV_GOARCH:     ${GO_ENV_GOARCH}";
echo "GO_ENV_GOOS:       ${GO_ENV_GOOS}";
echo "STATIC:            ${STATIC}";
echo "TIMESTAMP:         ${TIMESTAMP}";
echo "USE_CACHE:         ${USE_CACHE}";
echo "------------------ derived values";
echo "BIN_FULL_FILENAME: ${BIN_FULL_FILENAME}";
echo "BIN_FULL_PATH:     ${BIN_FULL_PATH}";
echo "BIN_FILENAME:      ${BIN_FILENAME}";
echo "BIN_SYMLINK_PATH:  ${BIN_SYMLINK_PATH}";
echo '----------------------------------------------------------------------------';
echo 'find help at - https://gitlab.com/usvc/images/ci/recipes#go-build-production';
echo '----------------------------------------------------------------------------';
echo '';

go mod graph;

# do the build
go build -mod=vendor -x -v ${OTHER_GO_BUILD_FLAGS} \
  -trimpath \
  -ldflags "\
    -X main.Commit=${GIT_COMMIT} \
    -X main.Version=${GIT_VERSION} \
    -X main.Timestamp=${TIMESTAMP} \
    -s -w \
    ${OTHER_LDFLAGS} \
  " \
  -o ${BIN_FULL_PATH} \
  ./cmd/${BIN_NAME};

# generate checksum at build time
sha256sum -b ${BIN_FULL_PATH} | cut -f 1 -d ' ' > ${BIN_FULL_PATH}.sha256;

# debug checks
ls -al ${BIN_PATH};

# create symlinks for local usage
cd ${BIN_PATH} \
  && rm -rf ./${BIN_FILENAME} \
  && ln -s ./${BIN_FULL_FILENAME} ./${BIN_FILENAME} \
  && rm -rf ./${BIN_FILENAME}.sha256 \
  && ln -s ./${BIN_FULL_FILENAME}.sha256 ./${BIN_FILENAME}.sha256 \
  && cd -;

# debug checks
ls -al ${BIN_PATH};
