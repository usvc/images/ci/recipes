#!/bin/sh
echo "upx version -------------------------------------------";
upx --version;

echo "sha256sum version -------------------------------------";
sha256sum --version;

# constants
export DEFAULT_BIN_PATH='./bin';
export DEFAULT_BIN_NAME='app';

# configurables
export BIN_EXT=${BIN_EXT};
export BIN_NAME=${BIN_NAME:-${DEFAULT_BIN_NAME}};
export BIN_PATH=${BIN_PATH:-${DEFAULT_BIN_PATH}};

# derived
export BIN_FULL_PATH="${BIN_PATH}/${BIN_NAME}${BIN_EXT}";
export BIN_TEMP_PATH="${BIN_PATH}/.${BIN_NAME}${BIN_EXT}";

# debug
echo "BIN_EXT:       ${BIN_EXT}";
echo "BIN_NAME:      ${BIN_NAME}";
echo "BIN_PATH:      ${BIN_PATH}";
echo "BIN_FULL_PATH: ${BIN_FULL_PATH}";
echo "BIN_TEMP_PATH: ${BIN_FULL_PATH}";
echo "find help at - https://gitlab.com/usvc/images/ci/recipes#compress"

# execute
echo "file before compression: $(ls -lah ${BIN_FULL_PATH})";
set +e;
rm -rf ${BIN_TEMP_PATH};
upx -9 -v -o ${BIN_TEMP_PATH} ${BIN_FULL_PATH};
set -e;
upx -t ${BIN_TEMP_PATH};
rm -rf ${BIN_FULL_PATH};
mv ${BIN_TEMP_PATH} ${BIN_FULL_PATH};
sha256sum -b ./bin/$(BIN_PATH) | cut -f 1 -d ' ' > ./bin/$(BIN_PATH).sha256

# debug
echo "file after compression: $(ls -lah ${BIN_FULL_PATH})";
