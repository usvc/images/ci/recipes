#!/bin/sh
echo "node version ------------------------------------------";
node -v;

echo "npm version -------------------------------------------";
npm -v;

echo "yarn version ------------------------------------------";
yarn -v;

# constants
export DEFAULT_PRODUCTION="false";
export DEFAULT_USE_YARN="false";

# configurables
export PRODUCTION=${PRODUCTION:-${DEFAULT_PRODUCTION}};
export USE_YARN=${USE_YARN:-${DEFAULT_USE_YARN}};

# derived

# debug
echo "PRODUCTION: ${PRODUCTION}";
echo "USE_YARN:   ${USE_YARN}";
echo "find help at - https://gitlab.com/usvc/images/ci/recipes#js-dependencies";

if [ "${USE_YARN}" = "true" ]; then
  if [ "${PRODUCTION}" = "true" ]; then
    yarn install --production=${PRODUCTION} --pure-lockfile --non-interactive;
  else
    yarn install;
  fi;
elif [ "${PRODUCTION}" = "true" ]; then
  NODE_ENV=production npm ci --only=production;
else
  npm ci;
fi;

