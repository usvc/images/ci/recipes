#!/bin/sh
echo "go version --------------------------------------------";
go version;

echo "go environment ----------------------------------------";
go env;

# constants
export DEFAULT_COVERPROFILE_PATH='./c.out';
export DEFAULT_TEST_PATH='./...';

# configurables
export COVERPROFILE_PATH=${COVERPROFILE_PATH:-${DEFAULT_COVERPROFILE_PATH}};
export TEST_PATH=${TEST_PATH:=${DEFAULT_TEST_PATH}};

# derived

# debug
printf -- 'debug: \n';
echo "COVERPROFILE_PATH: ${COVERPROFILE_PATH}";
echo "TEST_PATH: ${TEST_PATH}";
echo '----------------------------------------------------------------';
echo 'find help at - https://gitlab.com/usvc/images/ci/recipes#go-test';
echo '----------------------------------------------------------------';
echo '';

go test -v -mod=vendor "${TEST_PATH}" -cover -coverprofile "${COVERPROFILE_PATH}";
go tool cover -func "${COVERPROFILE_PATH}";
