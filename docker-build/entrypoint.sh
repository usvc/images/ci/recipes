#!/bin/sh
echo "docker version ----------------------------------------";
docker version;

echo "container-structure-test version ----------------------";
container-structure-test version;

# constants
export DEFAULT_DOCKERFILE_PATH='./deploy/Dockerfile';
export DEFAULT_DOCKER_CONTEXT_PATH='./';
export DEFAULT_DOCKER_IMAGE_TAG='latest';
export DEFAULT_OUTPUT_DIRECTORY='./build';
export DEFAULT_OUTPUT_FILENAME='output.tar.gz';
export DEFAULT_TEST_SPEC_PATH='./deploy/Dockerfile.yaml';

# configurables
export DOCKER_CONTEXT_PATH=${DOCKER_CONTEXT_PATH:-${DEFAULT_DOCKER_CONTEXT_PATH}};
export DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG:-${DEFAULT_DOCKER_IMAGE_TAG}};
export DOCKER_IMAGE_URL=${DOCKER_IMAGE_URL};
export DOCKERFILE_PATH=${DOCKERFILE_PATH:-${DEFAULT_DOCKERFILE_PATH}};
export OUTPUT_DIRECTORY=${OUTPUT_DIRECTORY:-${DEFAULT_OUTPUT_DIRECTORY}};
export OUTPUT_FILENAME=${OUTPUT_FILENAME:-${DEFAULT_OUTPUT_FILENAME}};
export TEST_SPEC_PATH=${TEST_SPEC_PATH:-${DEFAULT_TEST_SPEC_PATH}};

# derived
export DOCKER_SAVE_PATH="${OUTPUT_DIRECTORY}/${OUTPUT_FILENAME}";
export DOCKER_IMAGE_REFERENCE="${DOCKER_IMAGE_URL}:${DOCKER_IMAGE_TAG}";

# debug
echo "DOCKER_CONTEXT_PATH:    ${DOCKER_CONTEXT_PATH}";
echo "DOCKER_IMAGE_TAG:       ${DOCKER_IMAGE_TAG}";
echo "DOCKER_IMAGE_URL:       ${DOCKER_IMAGE_URL}";
echo "DOCKERFILE_PATH:        ${DOCKERFILE_PATH}";
echo "OUTPUT_FILENAME:        ${OUTPUT_FILENAME}";
echo "TEST_SPEC_PATH:         ${TEST_SPEC_PATH}";
echo "DOCKER_SAVE_PATH:       ${DOCKER_SAVE_PATH}";
echo "DOCKER_IMAGE_REFERENCE: ${DOCKER_IMAGE_REFERENCE}";
echo "find help at - https://gitlab.com/usvc/images/ci/recipes#docker-build"

set -e;
docker build --file ${DOCKERFILE_PATH} --tag ${DOCKER_IMAGE_REFERENCE} ${DOCKER_CONTEXT_PATH};
docker inspect ${DOCKER_IMAGE_REFERENCE};
docker history ${DOCKER_IMAGE_REFERENCE};
if [ -e "${TEST_SPEC_PATH}" ]; then 
  container-structure-test test --config ${TEST_SPEC_PATH} --image ${DOCKER_IMAGE_REFERENCE};
fi;
mkdir -p ${OUTPUT_DIRECTORY};
docker save --output ${DOCKER_SAVE_PATH} ${DOCKER_IMAGE_REFERENCE};
