#!/bin/sh
echo "go version --------------------------------------------";
go version;

echo "go environment ----------------------------------------";
go env;

# constants

# configurables

# derived

# debug
echo "find help at - https://gitlab.com/usvc/images/ci/recipes#go-dependencies"

go mod vendor -v;
go mod tidy -v;
go mod graph;

ls -al ./vendor;
