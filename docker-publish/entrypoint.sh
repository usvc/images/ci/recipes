#!/bin/sh
echo "docker version ----------------------------------------";
docker version;

echo "git version -------------------------------------------";
git --version;

# constants
export DEFAULT_DOCKER_IMAGE_TAG='latest';
export DEFAULT_DOCKER_REGISTRY_HOSTNAME='docker.io';
export DEFAULT_INPUT_DIRECTORY='./build';
export DEFAULT_INPUT_FILENAME='output.tar.gz';
export GIT_COMMIT=$(git rev-parse --verify HEAD);
export GIT_VERSION=$(git describe --tag $(git rev-list --tags --max-count=1));

# configurables
export DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG:-${DEFAULT_DOCKER_IMAGE_TAG}};
export DOCKER_IMAGE_URL=${DOCKER_IMAGE_URL};
export DOCKER_REGISTRY_HOSTNAME=${DOCKER_REGISTRY_HOSTNAME:-${DEFAULT_DOCKER_REGISTRY_HOSTNAME}};
export DOCKER_REGISTRY_USERNAME=${DOCKER_REGISTRY_USERNAME};
export DOCKER_REGISTRY_PASSWORD=${DOCKER_REGISTRY_PASSWORD};
export INPUT_DIRECTORY=${INPUT_DIRECTORY:-${DEFAULT_INPUT_DIRECTORY}};
export INPUT_FILENAME=${INPUT_FILENAME:-${DEFAULT_INPUT_FILENAME}};

# derived
export DOCKER_LOAD_PATH="${INPUT_DIRECTORY}/${INPUT_FILENAME}";
export DOCKER_IMAGE_REFERENCE="${DOCKER_IMAGE_URL}:${DOCKER_IMAGE_TAG}";

# debug
echo "DOCKER_IMAGE_TAG:         ${DOCKER_IMAGE_TAG}";
echo "DOCKER_IMAGE_URL:         ${DOCKER_IMAGE_URL}";
echo "DOCKER_REGISTRY_HOSTNAME: ${DOCKER_REGISTRY_HOSTNAME}";
echo "DOCKER_REGISTRY_USERNAME: ${DOCKER_REGISTRY_USERNAME}";
echo "DOCKER_REGISTRY_PASSWORD: ${DOCKER_REGISTRY_PASSWORD}";
echo "GIT_COMMIT:               ${GIT_COMMIT}";
echo "GIT_VERSION:              ${GIT_VERSION}";
echo "INPUT_DIRECTORY:          ${INPUT_DIRECTORY}";
echo "INPUT_FILENAME:           ${INPUT_FILENAME}";
echo "DOCKER_LOAD_PATH:         ${DOCKER_LOAD_PATH}";
echo "find help at - https://gitlab.com/usvc/images/ci/recipes#docker-publish"

set -e;
docker login ${DOCKER_REGISTRY_HOSTNAME} --username ${DOCKER_REGISTRY_USERNAME} --password ${DOCKER_REGISTRY_PASSWORD};
docker load --input ${DOCKER_LOAD_PATH};
docker push ${DOCKER_IMAGE_REFERENCE};
git fetch;
docker tag ${DOCKER_IMAGE_REFERENCE} ${DOCKER_IMAGE_URL}:${GIT_VERSION};
docker push ${DOCKER_IMAGE_URL}:${GIT_VERSION};
docker tag ${DOCKER_IMAGE_REFERENCE} ${DOCKER_IMAGE_URL}:${GIT_VERSION}-${GIT_COMMIT};
docker push  ${DOCKER_IMAGE_URL}:${GIT_VERSION}-${GIT_COMMIT};
docker logout;
rm -rf ~/.docker/config.json;
